#!/usr/bin/env ash

if [ "${1}" = "late" ]; then
  echo "Installing daemon for nvmevolume"
  cp -fv ./bc /tmpRoot/usr/sbin/bc
  chmod +x /tmpRoot/usr/sbin/bc
  cp -v  ./nvmevolume.sh /tmpRoot/usr/sbin/nvmevolume.sh
  chmod +x /tmpRoot/usr/sbin/nvmevolume.sh

  DEST="/tmpRoot/etc/systemd/system/nvmevolume.service"
  echo "[Unit]"                                    >${DEST}
  echo "Description=Enable M2 volume"             >>${DEST}
  echo "After=multi-user.target"                  >>${DEST}
  echo                                            >>${DEST}
  echo "[Service]"                                >>${DEST}
  echo "Type=oneshot"                             >>${DEST}
  echo "RemainAfterExit=true"                     >>${DEST}
  echo "ExecStart=/usr/sbin/nvmevolume.sh"        >>${DEST}
  echo                                            >>${DEST}
  echo "[Install]"                                >>${DEST}
  echo "WantedBy=multi-user.target"               >>${DEST}

  mkdir -vp /tmpRoot/lib/systemd/system/multi-user.target.wants
  ln -vsf /etc/systemd/system/nvmevolume.service /tmpRoot/lib/systemd/system/multi-user.target.wants/nvmevolume.service
fi
